.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:AdvancedTipsAndTricks:

Advanced: Tips & Tricks
#########################

Introduction
============

This section holds tips and tricks that don't fit anywhere else, or
are small enough that they don't deserve their own tutorial.

Creating a Custom Filter
========================

-  Open disk_out_ref.ex2.
-  **Apply**.
-  In the **Properties** tab, Set **Coloring** to **Temp**.
-  Select **Clip** filter.
-  Set **Clip type** to **Scalar**. (The Scalars and Values don’t
   matter right now)
-  **Apply**.
-  Select **Tools → Create Custom Filter**.
-  Name this filter ClipByScalar (IsoVolume).
-  Take the default inputs.
-  Take the default outputs.
-  Select the clip (in the left window), and pull down the pull down
   menu for Property.
-  Select Scalars.
-  Hit the blue **+** sign.
-  Do the same for **Value** and **Inside Out**.
-  Finish.
-  Now, delete the **Clip** filter in the pipeline browser.
-  Select **Filters → Alphabetical → ClipByScalar** (sometimes incorrectly know as an IsoVolume filter).
-  Turn Scalars to Temp
-  Enter a Value of 400.
-  **Apply**.

Temporal Statistics Filter
==========================

-  Open can.ex2.
-  **Apply**.
-  **Filters → Temporal → Temporal Statistics**.
-  **Apply**.
-  Set **Coloring** to **ACCL_average**.
-  Set **Coloring** to  **ACCL_maximum**.
-  Set **Coloring** to  **DISPL_average**.
-  You can now visually see average acceleration, maximum acceleration
   and average displacement of each cell.
-  To see the ranges of these variables over the whole mesh, look in the Information tab.

.. figure:: ../../images/Advanced_tips_and_tricks_2.png
   :width: 800px

Creating vectors from 2 or 3 scalars
====================================

-  Open can.ex2.
-  **Apply**.
-  Select **Filters → Common → Calculator**.
-  Build the vector equation using the iHat, jHat and kHat buttons on
   the calculator. For instance, this example will create a vector
   representing the acceleration in the X and Y plane.
-  Set **Result Array** Name to **ACCL-XY**.
-  Set **Expression** to **ACCL_X*iHat+ACCL_Y*jHat**.
-  Set **Coloring** to **ACCL-XY**.

Mesh quality
============

-  Select **Sources → Alphabetical → Sphere**.
-  In the **Properties** tab, set **Theta Resolution** and **Phi Resolution** to 50.
-  **Apply**.
-  Select **Filters → Alphabetical → Mesh Quality**. and use defaults.
-  **Apply**.
-  Next, we want to only look at those cells that are below some
   threshold of quality.
-  **Filters → Common → Threshold**.
-  Choose Scalars of “Quality”, and **Lower Threshold** of 2.3 and **Upper Threshold** of 10.
-  Turn visibility of **Sphere1** on.
-  Set **Representation → Wireframe**.
-  Set **Opacity** to 0.5.

.. figure:: ../../images/Advanced_tips_and_tricks_3.png
   :width: 800px

Backface styling
================

It is possible to change the backface style of a wire frame object.
-  Open can.ex2.
-  **Apply**.
-  Set **Coloring** to **ids**.
-  Set **Representation → Wireframe**.
-  In the Properties tab, Click Advanced.
-  Slide down a few pages until you find **Backface Styling**.
-  Set **Backface Representation → Cull Backface**.

.. figure:: ../../images/Advanced_tips_and_tricks_5.png
   :width: 800px

Animating a static vector field
===============================

-  If you have a vector field in your data, you can animate a static
   dataset.
-  Our goal is to create a set of streamlines from a vector field, place
   points on this set of streamlines, and animate the point down the
   streamlines. We will also add glyphs to the streamline points.
-  Open disk_out_ref.ex2.
-  **Apply**.
-  Click **-X**.
-  Select **Filters → Common → Stream tracer**. (We are already streamtracing on V).
-  Set **Seed Type** to Point Cloud.
-  Unckeck **Show sphere**.
-  Set **Opacity** to 0.3.
-  **Apply**.
-  Open **View → Color Map Editor**.
-  Click **Invert the transfer functions**.
-  Select **Filters → Common → Contour**.
-  Contour on **IntegrationTime**.
-  **Apply**.
-  Select **Filters → Common → Glyph**.
-  Set **Orientation Array** to **v**.
-  Set **Glyph Mode** to **All Points**.
-  Set **Scale factor** to 0.5.
-  Set **Coloring** to **v**.
-  **Apply**.
-  In the Pipeline browser, hide **Contour1**, and show **SteamTracer1** and **Contour1**.
-  **View → Animation View**.
-  Set **Mode** to **Sequence**.
-  Set **No. Frames** to **100**.
-  Change the pulldown box next to the blue **+** to be **Contour1**.
-  Click the blue **+**.
-  **Play**.

.. figure:: ../../images/Advanced_tips_and_tricks_7.png
   :width: 800px
